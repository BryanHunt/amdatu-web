/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.wink;

import java.util.Dictionary;
import java.util.Hashtable;

import javax.ws.rs.core.Application;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.log.LogService;
import org.slf4j.impl.StaticLoggerBinder;

/**
 * This is the OSGi activator for the Amdatu REST framework based on Apache Wink.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {
	/** PID by which the registered service can be configured. */
    public static final String CONFIG_PID = "org.amdatu.web.rest.wink";

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
    	manager.add(createComponent()
			.setImplementation(StaticLoggerBinder.getSingleton())
			.add(createServiceDependency()
				.setService(LogService.class)
				.setRequired(false)
			)
		);

        Dictionary<String, String> props = new Hashtable<String, String>();
        props.put(Constants.SERVICE_PID, CONFIG_PID);

        manager.add(createComponent()
            .setAutoConfig(Component.class, Boolean.FALSE)
            .setInterface(ManagedService.class.getName(), props)
            .setImplementation(WinkRegistrationServiceImpl.class)
            .add(createServiceDependency()
                .setService("(" + Constants.OBJECTCLASS + "=*)")
                .setCallbacks("onAdded", "onRemoved")
                .setRequired(false))
            .add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(false)
            )
        );
        
        manager.add(createComponent()
    		.setInterface(Application.class.getName(), null)
            .setImplementation(WinkApplication.class)
            .add(createServiceDependency()
        		.setService(LogService.class)
        		.setRequired(false)
    		)
		);
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }
}
