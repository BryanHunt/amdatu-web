/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.wink;

import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.osgi.service.log.LogService;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Application implementation that sets up the Jackson Provider
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class WinkApplication extends Application {

    private volatile LogService m_logService;

    @Override
    public Set<Object> getSingletons() {
        Set<Object> s = new HashSet<Object>();

        try {
            //If the Jackson XC bundle is installed we can use JAXB annotations.
            Class.forName("org.codehaus.jackson.xc.JaxbAnnotationIntrospector");
            s.add(new JacksonJaxbJsonProvider());
            m_logService.log(LogService.LOG_INFO, "Jackson JAX-RS provider configured with JAXB annotation support");
        } catch (Exception e) {
            JacksonJsonProvider jaxbProvider = new JacksonJsonProvider();
            s.add(jaxbProvider);

            m_logService.log(LogService.LOG_INFO, "Jackson JAX-RS provider configured");
        }

        return s;
    }
}
