/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.wink;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

import javax.servlet.Servlet;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Variant.VariantListBuilder;
import javax.ws.rs.ext.RuntimeDelegate;

import org.amdatu.web.rest.jaxrs.JaxRsSpi;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.apache.wink.common.internal.runtime.RuntimeDelegateImpl;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.log.LogService;

/**
 * This class is responsible for booting Wink as well as publication of new REST
 * services. Any service that declares a valid <code>Path</code> annotation on
 * its interfaces will be published as a REST servlet. The servlet alias is
 * determined by adding the resource Path value to the REST base path.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class WinkRegistrationServiceImpl implements ManagedService {
    
    static final String REQUEST_PROCESSOR_ATTRIBUTE = "requestProcessorAttribute";
    
    private static final String CONFIG_REST_PATH = "wink.rest.path";
    private static final String DEFAULT_PREFIX = "";

    private static final String INIT_APPLICATION_CONFIG_LOCATION = "init.applicationConfigLocation";
    private static final String INIT_REQUEST_PROCESSOR_ATTRIBUTE = "init.".concat(REQUEST_PROCESSOR_ATTRIBUTE);
    private static final String CONTEXT_ID = "contextId";
    private static final String ALIAS = "alias";

    // Location of application properties in this bundle
    private static final String APP_PROPS = "/conf/application.properties";

    // Map of registered REST service components
    private final ConcurrentHashMap<ServiceReference, ComponentService> m_servletComponents;
    private final AtomicReference<String> m_commonPrefixRef;

    private Component m_spiComponent;

    private volatile Application m_application;
    
    // Services injected by dependency manager
    private volatile DependencyManager m_dependencyManager;
    private volatile LogService m_logService;

    /**
     * Creates a new {@link WinkRegistrationServiceImpl} instance.
     */
    public WinkRegistrationServiceImpl() {
    	m_servletComponents = new ConcurrentHashMap<ServiceReference, ComponentService>();
    	m_commonPrefixRef = new AtomicReference<String>(DEFAULT_PREFIX);
	}

    public void destroy() {
        removeSpiService();
        for (ComponentService servletPair : m_servletComponents.values()) {
            m_dependencyManager.remove(servletPair.m_component);
        }
        m_servletComponents.clear();
    }

    public void init() {
        setRuntimeDelegate();
        registerSpiService();
    }

    public void onAdded(ServiceReference serviceReference, Object service) {
        if (service.getClass().getAnnotation(Path.class) != null) {
            addRestServlet(serviceReference, service);
        }
        else if (Application.class.isAssignableFrom(service.getClass())) {
            addApplicationClass(serviceReference, (Application) service);
        }
    }

    public void onRemoved(ServiceReference serviceReference, Object service) {
        if (service.getClass().getAnnotation(Path.class) != null) {
            removeRestServlet(serviceReference, service);
        } else if (service instanceof Application) {
            m_application = null;
            reInitializeServlets();
        }
    }

    @SuppressWarnings("rawtypes")
    public void updated(Dictionary properties) throws ConfigurationException {
        String path = DEFAULT_PREFIX;
        
        if (properties != null) {
            Object value = properties.get(CONFIG_REST_PATH);
            if (value == null) {
                throw new ConfigurationException(CONFIG_REST_PATH, "Configuration value is missing");
            }
            if (!(value instanceof String)) {
                throw new ConfigurationException(CONFIG_REST_PATH, "Configuration value must be String");
            }

            path = (String) value;

            if ((path.length() > 0) && (!path.startsWith("/") || path.endsWith("/"))) {
                throw new ConfigurationException(CONFIG_REST_PATH, "Configuration value should start with / and not end with /");
            }
        }

        String oldPath;
        do {
        	oldPath = m_commonPrefixRef.get();
        } while (!m_commonPrefixRef.compareAndSet(oldPath, path));

        m_logService.log(LogService.LOG_INFO, "REST path updated to '" + path + "'");
        
        updateRestServlets();
    }

    private void addApplicationClass(ServiceReference serviceReference, Application service) {
        m_application = service;
        reInitializeServlets();

        m_logService.log(LogService.LOG_DEBUG, "JAX-RS Application class registered: '" + service.getClass().getName() + "'");
    }

    private void addRestServlet(ServiceReference serviceReference, Object service) {
        Dictionary<String, String> initParams = new Hashtable<String, String>();
        // Add a "fake" application config location to suppress a warning getting logged...
        initParams.put(INIT_APPLICATION_CONFIG_LOCATION, APP_PROPS);

        String restPath = service.getClass().getAnnotation(Path.class).value();
        String prefix = m_commonPrefixRef.get();
        String servletAlias = prefix.concat(restPath.startsWith("/") ? restPath : "/".concat(restPath));
        
        // Makes the request processor unique per servlet-alias...
        initParams.put(INIT_REQUEST_PROCESSOR_ATTRIBUTE, servletAlias);
        initParams.put(ALIAS, servletAlias);

        String contextId = getStringProperty(serviceReference, CONTEXT_ID);
        if (!"".equals(contextId)) {
            initParams.put(CONTEXT_ID, contextId);
        }

        WinkRestServlet restServlet = new WinkRestServlet(prefix, service, m_application);

        Component servletComponent = m_dependencyManager.createComponent()
                .setInterface(Servlet.class.getName(), initParams)
                .setImplementation(restServlet)
                .setCallbacks("_init", "start", "stop", "_destroy");

        // threadsafe
        if (m_servletComponents.putIfAbsent(serviceReference, new ComponentService(servletComponent, service)) == null) {
            m_dependencyManager.add(servletComponent);
            m_logService.log(LogService.LOG_DEBUG, "Wink application registered REST servlet '" + servletAlias + "'");
        }
        else {
            m_logService.log(LogService.LOG_ERROR, "Duplicate ServiceReference in callback: " + serviceReference);
        }
    }

    private String getStringProperty(ServiceReference ref, String key) {
        Object value = ref.getProperty(key);
        return (value instanceof String) ? (String) value : "";
    }

    private void registerSpiService() {
        Component comp = m_dependencyManager.createComponent()
                .setInterface(JaxRsSpi.class.getName(), null)
                .setImplementation(new JaxRsSpi() {
                });
        m_dependencyManager.add(comp);
        m_spiComponent = comp;
    }

    private void reInitializeServlets() {
        Map<ServiceReference, ComponentService> components = new HashMap<ServiceReference, ComponentService>();
        for(ServiceReference ref : m_servletComponents.keySet()) {
            m_dependencyManager.remove(m_servletComponents.get(ref).m_component);
            components.put(ref, m_servletComponents.get(ref));
        }

        m_servletComponents.clear();

        for(ServiceReference ref : components.keySet()) {
            addRestServlet(ref, components.get(ref).m_service);
        }
    }

    private void removeRestServlet(ServiceReference serviceReference, Object service) {
        // threadsafe
        Component servletComponent = m_servletComponents.remove(serviceReference).m_component;
        if (servletComponent != null) {
            m_dependencyManager.remove(servletComponent);
            m_logService.log(LogService.LOG_DEBUG, "Wink application unregistered REST servlet");
        } else {
            m_logService.log(LogService.LOG_ERROR, "Unknown ServiceReference in callback: " + serviceReference);
        }
    }

    private void removeSpiService() {
        m_dependencyManager.remove(m_spiComponent);
    }

    /**
     * Sets the runtime delegate, used to create instances of desired endpoint classes.
     */
    private void setRuntimeDelegate() {
        // FIXME: OK, this is nasty, but necessary. Without this piece of code a NoClassDefFoundError
        // is thrown during servlet initialization on the class com.sun.ws.rs.ext.RuntimeDelegateImpl.
        // The reason for this is that JAX-RS by default delegates to this class if no other RuntimeDelegate
        // implementation could be found. Apache Wink does come with its own RuntimeDelegate, but during
        // initialization of this instance, RuntimeDelegate.getInstance() is invoked which causes a method
        // call to com.sun.ws.rs.ext.RuntimeDelegateImpl (see EntityTagMatchHeaderDelegate).
        // Other frameworks face similar issues using JAX-RS, see for example this URL for the exact same
        // problem in the Restlet framework: http://www.mail-archive.com/discuss@restlet.tigris.org/msg07539.html
        // The nasty fix is to set some dummy RuntimeDelegate first, then set the Wink RuntimeDelegateImpl
        RuntimeDelegate.setInstance(new RuntimeDelegate() {
            @Override
            public <T> T createEndpoint(Application arg0, Class<T> arg1) throws IllegalArgumentException,
                UnsupportedOperationException {
                return null;
            }

            @Override
            public <T> HeaderDelegate<T> createHeaderDelegate(Class<T> arg0) {
                return null;
            }

            @Override
            public ResponseBuilder createResponseBuilder() {
                return null;
            }

            @Override
            public UriBuilder createUriBuilder() {
                return null;
            }

            @Override
            public VariantListBuilder createVariantListBuilder() {
                return null;
            }
        });
        RuntimeDelegate.setInstance(new RuntimeDelegateImpl());
    }

    private void updateRestServlets() {
        m_logService.log(LogService.LOG_DEBUG, "Updating REST servlets");
        for (Map.Entry<ServiceReference, ComponentService> entry : m_servletComponents.entrySet()) {
            removeRestServlet(entry.getKey(), entry.getValue().m_service);
            addRestServlet(entry.getKey(), entry.getValue().m_service);
        }
    }

    private static final class ComponentService {
        final Component m_component;
        final Object m_service;
        
        public ComponentService(Component component, Object service) {
            m_component = component;
            m_service = service;
        }
    }
}
