/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.wink;

import static org.amdatu.web.rest.wink.WinkRegistrationServiceImpl.REQUEST_PROCESSOR_ATTRIBUTE;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Application;

import org.apache.wink.server.internal.RequestProcessor;
import org.apache.wink.server.internal.servlet.RestServlet;
import org.apache.wink.server.utils.RegistrationUtils;

/**
 * Extends the default Apache Wink {@link RestServlet} to handle multiple processors and
 * wrap incoming requests to match resources.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class WinkRestServlet extends RestServlet {
	private static final long serialVersionUID = -4472025056797521781L;

	private final String m_commonPrefix;
    private final Object m_service;
    private final Application m_application;

    // Indicates if this REST servlet has been initialized
    private volatile boolean m_initialized;
    
	/**
	 * Creates a new {@link WinkRestServlet} instance.
	 * 
	 * @param commonPrefix the common prefix used for all Wink servlets;
	 * @param service the JAX-RS service to expose to Wink.
	 */
    public WinkRestServlet(String commonPrefix, Object service) {
        this(commonPrefix, service, null);
    }

	/**
	 * Creates a new {@link WinkRestServlet} instance.
	 * 
	 * @param commonPrefix the common prefix used for all Wink servlets;
	 * @param service the JAX-RS service to expose to Wink;
	 * @param application the Wink-application to register.
	 */
    public WinkRestServlet(String commonPrefix, Object service, Application application) {
        super();
        
        m_commonPrefix = commonPrefix;
        m_service = service;
        m_application = application;
        
        m_initialized = false;
    }

    @Override
    protected void service(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        // The next line seems to do nothing, but it does. Without requesting parameter names here, using both
        // @FormParam JAX-RS annotations in a REST resource as well as getParameterNames() in the same @PUT
        // or @POST annotated method will not work (getParameterNames() will return an empty map without the
        // posted parameters). This is because the InputStream in that case is read by the FormParam handler
        // first, which does not write the read from parameters to the parameter map. The other way around
        // works fine; when the input stream has already been read and written to the parameter map the
        // FormParam handler binds the values from this map instead of reading them from the input stream.
        httpServletRequest.getParameterNames();
        // We allow all Wink-servlets to be prefixed with a common alias. Due to this, we need to do some 
        // magic in the servlet request mapping in order to let Wink behave properly. 
        // See more information below...
        super.service(new WinkResourceRequestWrapper(httpServletRequest, m_commonPrefix), httpServletResponse);
    }

    /**
     * Overridden in order to have a request processor to be created <b>after</b> this 
     * servlet itself is initialized.
     */
    @Override
    protected RequestProcessor getRequestProcessor() {
        if (!m_initialized) {
            // Override this method to enforce creation of a new requestprocessor whenever a servlet is created
            m_initialized = true;

            return null;
        }
        else {
            return super.getRequestProcessor();
        }
    }

    /**
     * Overridden in order to let the servlet to initialize itself using the non-OSGi 
     * classloader (it might dynamically load classes unknown to OSGi). 
     */
    @Override
    public void init() throws ServletException {
        // Perform the init with the bundle classloader set as context classloader on
        // the current thread, as the ClassUtils.loadClass method of Wink will try
        // to load classes from this classloader.
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
            super.init();
        }
        finally {
            Thread.currentThread().setContextClassLoader(cl);
        }
    }

    /**
     * Lazy registration of the application and actual JAX-RS service.
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
        // Determine the name of the request processor as provided in the init parameters 
        // of the servlet (see WinkRegistrationServiceImpl#addRestServlet)...
        String reqProcessorAttr = config.getInitParameter(REQUEST_PROCESSOR_ATTRIBUTE);
        
		if (m_application != null) {
            RegistrationUtils.registerApplication(m_application, getServletContext(), reqProcessorAttr);
        }
        RegistrationUtils.registerInstances(getServletContext(), reqProcessorAttr, m_service);
    }

    /**
     * Wraps {@link HttpServletRequestWrapper} to "remove" the common prefix (as defined 
     * in {@link WinkRegistrationServiceImpl}) from all servlet requests.
     */
    private static final class WinkResourceRequestWrapper extends HttpServletRequestWrapper {
    	private final String m_commonPrefix;

    	public WinkResourceRequestWrapper(HttpServletRequest req, String commonPrefix) {
            super(req);

            m_commonPrefix = commonPrefix;
        }
        
    	/**
    	 * We expose the Wink-servlets with a (optional) prefix. Because of this, we 
    	 * need to tell Wink what prefix it should consider to be part of the servlet 
    	 * part. Otherwise, Wink will use the alias of the registered servlet as servlet
    	 * path, which causes it to no longer resolve the {\@Path} annotations at the 
    	 * class level.
    	 */
        @Override
        public String getServletPath() {
        	// Returns the common prefix under which the servlet is registered...
        	return m_commonPrefix;
        }
    }
}
