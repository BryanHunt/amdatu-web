/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler.itest;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

import org.apache.felix.dm.tracker.ServiceTracker;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleListener;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationEvent;
import org.osgi.service.cm.ConfigurationListener;

public class ResourceHandlerTest extends TestCase implements BundleListener {
    private static final int HTTP_OK = 200;

    private static final int HTTP_NOT_FOUND = 404;
    private static final String PREFIX = "http://localhost:8080/";

    private static final String URL_HELLO_WORLD = "bundle1/helloworld.txt";
    private static final String URL_FAREWELL_WORLD = "bundle2/farewellworld1.txt";
    private static final String URL_FAREWELL_AGAIN = "bundle2/farewellworld2.txt";
    private static final String URL_FAREWELL_HELLO_WORLD = "bundle1/bundle2/farewellworld.txt";
    private static final String URL_BUNDLE4_ROOT_WITHOUT_DEFAULT = "bundle4/";
    private static final String URL_BUNDLE4_ROOT_WITH_DEFAULT = "bundle4/default.txt";
    private static final String URL_BUNDLE4_A_WITHOUT_SLASH = "bundle4/a";
    private static final String URL_BUNDLE4_A_WITHOUT_DEFAULT = "bundle4/a/";
    private static final String URL_BUNDLE4_A_WITH_DEFAULT = "bundle4/a/a.txt";
    private static final String URL_BUNDLE4_B_WITHOUT_DEFAULT = "bundle4/b/";
    private static final String URL_BUNDLE4_B_WITH_DEFAULT = "bundle4/b/default.txt";
    private static final String URL_BUNDLE4_B_NON_EXISTING = "bundle4/b/nonExisting.txt";
    private static final String URL_BUNDLE4_C_WITHOUT_SLASH = "bundle4/c";
    private static final String URL_BUNDLE4_C_C = "bundle4/c/c.txt";
    private static final String URL_BUNDLE4_D_WITHOUT_SLASH = "bundle4/d";
    private static final String URL_BUNDLE4_D_NON_EXISTING = "bundle4/d/nonExisting.txt";
    private static final String URL_BUNDLE5_ROOT_WITHOUT_DEFAULT = "bundle5/";
    private static final String URL_BUNDLE5_ROOT_WITH_DEFAULT = "bundle5/default.txt";
    private static final String URL_WOOT = "http://localhost:8080/bundle3/woot.txt";

    private final BundleContext m_context;
    private volatile BundleStateLatch m_latch;

    private Bundle m_helloBundle;
    private Bundle m_farewellBundle;
    private Bundle m_resourceHandler;
    private Bundle m_bundleOwnContext;

    public ResourceHandlerTest() {
        m_context = FrameworkUtil.getBundle(getClass()).getBundleContext();
    }

    public void bundleChanged(BundleEvent event) {
        if (m_latch != null) {
            m_latch.countDownOnMatch(event);
        }
    }

    public void testDefaultCacheHeadersOk() throws Exception {
        String headerField = getHeader(URL_HELLO_WORLD, "Cache-Control");
        assertEquals("max-age=604800, must-revalidate", headerField);
    }

    public void testDefaultPageAtRootOk() throws Exception {
        assertEquals("default", getContents(URL_BUNDLE4_ROOT_WITH_DEFAULT));
        assertEquals("default", getContents(URL_BUNDLE4_ROOT_WITHOUT_DEFAULT));
    }

    public void testDefaultPageAtSubDirOk() throws Exception {
        assertEquals("a", getContents(URL_BUNDLE4_A_WITH_DEFAULT));
        assertEquals("a", getContents(URL_BUNDLE4_A_WITHOUT_SLASH));
        assertEquals("a", getContents(URL_BUNDLE4_A_WITHOUT_DEFAULT));

        assertEquals("b_default", getContents(URL_BUNDLE4_B_WITH_DEFAULT));
        assertEquals("b_default", getContents(URL_BUNDLE4_B_WITHOUT_DEFAULT));

        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_BUNDLE4_B_NON_EXISTING));

        assertEquals("c", getContents(URL_BUNDLE4_C_C));
        assertEquals("default", getContents(URL_BUNDLE4_C_WITHOUT_SLASH));

        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_BUNDLE4_D_NON_EXISTING));
        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_BUNDLE4_D_WITHOUT_SLASH));
    }

    public void testDefaultPageOnlyForWebResourceV1dot1OrLater() throws Exception {
        // bundle 5 uses v1.0 of X-Web-Resource, hence its defined default pages should not be picked up...
        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_BUNDLE5_ROOT_WITHOUT_DEFAULT));
        // explicitly mentioned files should be picked up normally...
        assertEquals("default", getContents(URL_BUNDLE5_ROOT_WITH_DEFAULT));
    }

    public void testDeleteCacheHeaderConfigOk() throws Exception {
        Properties props = new Properties();
        props.put("cache.timeout", "10");

        String headerField;

        configureService("org.amdatu.web.resourcehandler", props);

        headerField = getHeader(URL_HELLO_WORLD, "Cache-Control");
        assertEquals("max-age=10, must-revalidate", headerField);

        configureService("org.amdatu.web.resourcehandler", null);

        headerField = getHeader(URL_HELLO_WORLD, "Cache-Control");
        assertEquals("max-age=604800, must-revalidate", headerField);
    }

    public void testDisabledCacheHeadersOk() throws Exception {
        Properties props = new Properties();
        props.put("cache.timeout", -1L);

        configureService("org.amdatu.web.resourcehandler", props);

        String headerField = getHeader(URL_HELLO_WORLD, "Cache-Control");
        assertNull(headerField);
    }

    public void testHelloWorld() throws Exception {
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
    }

    public void testMultiUrls() throws Exception {
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_AGAIN));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_HELLO_WORLD));
    }

    public void testOwnContext() throws Exception {
        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
    }

    public void testRestartResource() throws Exception {
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));

        stopBundle(m_helloBundle);

        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));

        startBundle(m_helloBundle);

        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));
    }

    public void testRestartResourceHandler() throws Exception {
        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));

        stopBundle(m_resourceHandler);

        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_WOOT));
        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_FAREWELL_WORLD));

        startBundle(m_resourceHandler);
        waitUntilResourcesAreAvailable();

        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));
    }

    public void testStopOwnContext() throws Exception {
        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));

        stopBundle(m_bundleOwnContext);

        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));

        startBundle(m_bundleOwnContext);

        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));
    }

    public void testUpdateCacheHeaderConfigOk() throws Exception {
        Properties props = new Properties();
        props.put("cache.timeout", "10");

        configureService("org.amdatu.web.resourcehandler", props);

        String headerField = getHeader(URL_HELLO_WORLD, "Cache-Control");
        assertEquals("max-age=10, must-revalidate", headerField);
    }

    /**
     * Set up for each individual test.
     */
    @Override
    protected void setUp() throws Exception {
        Bundle[] bundles = m_context.getBundles();
        for (Bundle bundle : bundles) {
            String symbolicName = bundle.getSymbolicName();
            if ("org.amdatu.web.resourcehandler".equals(symbolicName)) {
                m_resourceHandler = bundle;
            }
            if ("org.amdatu.web.resourcehandler.itest.bundle1".equals(symbolicName)) {
                m_helloBundle = bundle;
            }
            if ("org.amdatu.web.resourcehandler.itest.bundle2".equals(symbolicName)) {
                m_farewellBundle = bundle;
            }
            if ("org.amdatu.web.resourcehandler.itest.bundle3".equals(symbolicName)) {
                m_bundleOwnContext = bundle;
            }
        }

        // Sanity check before entering any test...
        assertNotNull("...itest.bundle1 does not appear to be installed?!", m_helloBundle);
        assertNotNull("...itest.bundle2 does not appear to be installed?!", m_farewellBundle);
        assertNotNull("...itest.bundle3 does not appear to be installed?!", m_bundleOwnContext);
        assertNotNull("...web.resourcehandler does not appear to be installed?!", m_resourceHandler);

        // Wait until the resources are all registered...
        waitUntilResourcesAreAvailable();

        // Listen to all bundle events...
        m_context.addBundleListener(this);
    }

    /**
     * Tear down for each individual test.
     */
    @Override
    protected void tearDown() throws Exception {
        // Stop listening to bundle events...
        m_context.removeBundleListener(this);
        // Make sure we don't reuse old latches...
        m_latch = null;
    }

    private void configureService(final String pid, Properties props) throws Exception {
        ServiceTracker tracker = new ServiceTracker(m_context, ConfigurationAdmin.class.getName(), null);
        tracker.open();

        ServiceRegistration reg = null;

        try {
            ConfigurationAdmin configAdmin = (ConfigurationAdmin) tracker.waitForService(TimeUnit.SECONDS.toMillis(5));
            assertNotNull("No configuration admin service found?!", configAdmin);

            final CountDownLatch latch = new CountDownLatch(1);
            final int configEvent = (props != null) ? ConfigurationEvent.CM_UPDATED : ConfigurationEvent.CM_DELETED;

            Configuration config = configAdmin.getConfiguration(pid, null);

            reg = m_context.registerService(ConfigurationListener.class.getName(), new ConfigurationListener() {
                @Override
                public void configurationEvent(ConfigurationEvent event) {
                    if (pid.equals(event.getPid()) && event.getType() == configEvent) {
                        latch.countDown();
                    }
                }
            }, null);

            if (props != null) {
                config.update(props);
            } else {
                config.delete();
            }

            assertTrue("Configuration not provisioned in time!", latch.await(5, TimeUnit.SECONDS));
        } finally {
            if (reg != null) {
                reg.unregister();
            }

            tracker.close();
        }
    }

    /**
     * @return the HTTP response code for the given URL, e.g., 200 or 404.
     */
    private String getContents(String urlToRead) throws Exception {
        URL url = toURL(urlToRead);

        HttpURLConnection conn = null;
        InputStream is = null;

        try {
            conn = (HttpURLConnection) url.openConnection();

            int rc = conn.getResponseCode();
            if (rc != HTTP_OK) {
                return null;
            }
            is = conn.getInputStream();

            return slurpAsString(is);
        } finally {
            if (is != null) {
                is.close();
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    /**
     * @return the HTTP header field, can be <code>null</code>.
     */
    private String getHeader(String urlToRead, String headerName) throws Exception {
        URL url = toURL(urlToRead);

        HttpURLConnection conn = null;

        try {
            conn = (HttpURLConnection) url.openConnection();

            int rc = conn.getResponseCode();
            assertEquals(HTTP_OK, rc);

            return conn.getHeaderField(headerName);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    /**
     * @return the HTTP response code for the given URL, e.g., 200 or 404.
     */
    private int getResponseCode(String urlToRead) throws Exception {
        URL url = toURL(urlToRead);

        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            int rc = conn.getResponseCode();
            return rc;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    private String slurpAsString(InputStream is) throws IOException {
        Scanner scanner = null;

        try {
            scanner = new Scanner(is, "UTF-8");
            scanner.useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : null;
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
    }

    /**
     * Starts the given bundle and waits until the framework notifies all
     * listeners that it is started.
     */
    private void startBundle(Bundle bundle) throws Exception {
        m_latch = new BundleStateLatch(bundle, BundleEvent.STARTED);

        bundle.start();

        assertTrue("Failed to start bundle " + bundle, m_latch.await());
    }

    /**
     * Stops the given bundle and waits until the framework notifies all
     * listeners that it is stopped.
     */
    private void stopBundle(Bundle bundle) throws Exception {
        m_latch = new BundleStateLatch(bundle, BundleEvent.STOPPED);

        bundle.stop();

        assertTrue("Failed to stop bundle " + bundle, m_latch.await());
    }

    private URL toURL(String url) throws MalformedURLException {
        if (!url.startsWith(PREFIX)) {
            url = PREFIX.concat(url);
        }
        return new URL(url);
    }

    /**
     * Checks whether all desired resources are present at start up.
     */
    private void waitUntilResourcesAreAvailable() throws Exception {
        for (int i = 0; i < 10; i++) {
            try {
                if ((getResponseCode(URL_HELLO_WORLD) == HTTP_OK) && (getResponseCode(URL_WOOT) == HTTP_OK)
                    && (getResponseCode(URL_FAREWELL_WORLD) == HTTP_OK)
                    && (getResponseCode(URL_FAREWELL_HELLO_WORLD) == HTTP_OK)
                    && (getResponseCode(URL_FAREWELL_AGAIN) == HTTP_OK)) {
                    return;
                }
            } catch (IOException ex) {
                // Ignore for now...
            }
            TimeUnit.MILLISECONDS.sleep(100L);
        }
        fail("Web resources were not found after reasonable timeout!");
    }

    /**
     * Provides a custom {@link CountDownLatch} that allows it to count down in
     * case a bundle event for a specified bundle with a specified bundle
     * event-type is given.
     */
    static class BundleStateLatch {
        private final CountDownLatch m_latch;
        private final Bundle m_bundle;
        private final int m_type;

        public BundleStateLatch(Bundle bundle, int type) {
            m_latch = new CountDownLatch(1);
            m_bundle = bundle;
            m_type = type;
        }

        /**
         * Awaits until this latch reaches a count of zero or when 5 seconds
         * are passed, whichever comes first.
         */
        public boolean await() throws InterruptedException {
            return m_latch.await(5, TimeUnit.SECONDS);
        }

        /**
         * Counts down in case the given bundle event matches the contained
         * bundle and type.
         * 
         * @param event the bundle event to test, cannot be <code>null</code>.
         * @return <code>true</code> if the event matched, <code>false</code> otherwise.
         */
        public boolean countDownOnMatch(BundleEvent event) {
            if (event.getBundle() == m_bundle && event.getType() == m_type) {
                m_latch.countDown();
                return true;
            }
            return false;
        }
    }
}
