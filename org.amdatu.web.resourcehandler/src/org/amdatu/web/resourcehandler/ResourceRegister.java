/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler;

import static org.amdatu.web.resourcehandler.Constants.CACHE_TIMEOUT_DISABLED;
import static org.amdatu.web.resourcehandler.Constants.ONE_WEEK_IN_SECONDS;

import java.util.Dictionary;

import org.apache.felix.dm.Component;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.http.HttpContext;
import org.osgi.service.http.HttpService;
import org.osgi.service.log.LogService;

public class ResourceRegister implements ManagedService {
    /** The time (in seconds) to sent back for cache headers. */
    private static final String KEY_CACHE_TIMEOUT = "cache.timeout";

    private final ResourceEntry m_entry;
    private final DefaultPages m_defaultPages;
    // Injected by Felix DM...
    private volatile BundleContext m_context;
    private volatile HttpService m_httpService;
    private volatile HttpContext m_httpContext;
    private volatile LogService m_log;
    // The registered resource-servlet service...
    private volatile ResourceServlet m_servlet;

    public ResourceRegister(ResourceEntry entries, DefaultPages defaultPages) {
        m_entry = entries;
        m_defaultPages = defaultPages;
    }

    /**
     * Called by Felix DM upon destruction of this component, and causes a
     * deregistration of all registered resources.
     */
    public void destroy(Component component) {
        m_servlet = null;

        try {
            m_httpService.unregister(m_entry.getAlias());
        }
        catch (IllegalArgumentException exception) {
            m_log.log(LogService.LOG_WARNING, "Resource deregistration failed for " + m_entry.getAlias() + " ("
                + m_entry.getPaths() + ")", exception);
        }
    }

    /**
     * Called by Felix DM upon initialization of this component, which will
     * cause the resources to be registered.
     */
    public void init(Component component) {
        m_servlet = new ResourceServlet(m_context.getBundle(), m_entry, m_defaultPages);

        try {
            m_httpService.registerServlet(m_entry.getAlias(), m_servlet, null, m_httpContext);
        }
        catch (Exception exception) {
            m_log.log(LogService.LOG_WARNING,
                "Resource registration failed for " + m_entry.getAlias() + " (" + m_entry.getPaths() + ")", exception);
        }
    }

    @Override
    @SuppressWarnings("rawtypes")
    public void updated(Dictionary properties) throws ConfigurationException {
        if (m_servlet == null) {
            return;
        }

        long cacheTimeout = ONE_WEEK_IN_SECONDS;

        if (properties != null) {
            Object value = properties.get(KEY_CACHE_TIMEOUT);
            if (value != null) {
                if (value instanceof Number) {
                    cacheTimeout = ((Number) value).longValue();
                }
                else {
                    String strValue = value.toString().trim();

                    try {
                        cacheTimeout = Long.parseLong(strValue);
                    }
                    catch (NumberFormatException e) {
                        throw new ConfigurationException(KEY_CACHE_TIMEOUT, "Invalid numeric value!");
                    }
                }
            }
        }
        // Any negative value is used to disable the cache timeout...
        if (cacheTimeout < 0) {
            cacheTimeout = CACHE_TIMEOUT_DISABLED;
        }

        m_servlet.setCacheTimeout(cacheTimeout);

        if (cacheTimeout == CACHE_TIMEOUT_DISABLED) {
            info("Cache timeout for resources disabled...");
        }
        else {
            info("Cache timeout for resources set to " + cacheTimeout + " seconds...");
        }
    }

    void info(String message) {
        if (m_log != null) {
            m_log.log(LogService.LOG_INFO, message);
        }
    }
}
