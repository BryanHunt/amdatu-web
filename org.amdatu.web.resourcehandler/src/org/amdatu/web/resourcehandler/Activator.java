/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler;

import static org.amdatu.web.resourcehandler.Constants.WEB_RESOURCE_FILTER;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.http.HttpService;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {
    private static final String PID = "org.amdatu.web.resourcehandler";

	@Override
	public void destroy(BundleContext context, DependencyManager manager) throws Exception {
		// not needed
	}

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
	    Properties props = new Properties();
	    props.put(Constants.SERVICE_PID, PID);
	    
		manager.add(createComponent()
		    .setInterface(ManagedService.class.getName(), props)
			.setImplementation(ResourceHandler.class)
			.add(createBundleDependency()
					.setStateMask(Bundle.ACTIVE)
					.setFilter(WEB_RESOURCE_FILTER)
					.setCallbacks("addResourceBundle", "removeResourceBundle"))
			.add(createServiceDependency().setService(HttpService.class).setRequired(true))
			.add(createServiceDependency().setService(LogService.class).setRequired(false))
		);
	}
}
