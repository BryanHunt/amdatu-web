/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler;

import static org.amdatu.web.resourcehandler.Constants.CONTEXTID;
import static org.amdatu.web.resourcehandler.Constants.WEB_RESOURCE_DEFAULT_PAGE;
import static org.amdatu.web.resourcehandler.Constants.WEB_RESOURCE_KEY;
import static org.amdatu.web.resourcehandler.Constants.WEB_RESOURCE_VERSION_1_1;
import static org.amdatu.web.resourcehandler.Constants.WEB_RESOURCE_VERSION_KEY;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.amdatu.web.resourcehandler.util.DefaultPageParser;
import org.amdatu.web.resourcehandler.util.InvalidEntryException;
import org.amdatu.web.resourcehandler.util.ResourceKeyParser;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.Version;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.http.HttpContext;
import org.osgi.service.http.HttpService;
import org.osgi.service.log.LogService;

public class ResourceHandler implements ManagedService {

    private final ConcurrentMap<Bundle, DependencyManager> m_components;
    // Injected by Felix DM...
    private volatile LogService m_log;

    public ResourceHandler() {
        m_components = new ConcurrentHashMap<Bundle, DependencyManager>();
    }

    /**
     * Adds the resources of the given bundle to a specified or the default
     * HttpContext.
     * 
     * @param bundle
     *        the bundle to add the resources for, cannot be
     *        <code>null</code>.
     * @throws InvalidEntryException
     *         in case the bundle specified an invalid web resource key.
     */
    @SuppressWarnings("unchecked")
    public void addResourceBundle(Bundle bundle) throws InvalidEntryException {
        Dictionary<String, Object> headers = bundle.getHeaders();

        Version version = new Version((String) headers.get(WEB_RESOURCE_VERSION_KEY));

        String key = (String) headers.get(WEB_RESOURCE_KEY);
        ResourceEntryMap entryMap = ResourceKeyParser.getEntries(key);

        DefaultPages defaultPages = new DefaultPages();
        // As of version 1.1, we've added support for defining default pages. 
        // So we need to versions that are equal or greater than 1.1...
        if (WEB_RESOURCE_VERSION_1_1.compareTo(version) <= 0) {
            key = (String) headers.get(WEB_RESOURCE_DEFAULT_PAGE);
            defaultPages = DefaultPageParser.parseDefaultPages(key);
        }

        for (String contextId : entryMap.getContextIDs()) {
            List<ResourceEntry> entries = entryMap.getEntry(contextId);

            DependencyManager manager = new DependencyManager(bundle.getBundleContext());

            for (ResourceEntry entry : entries) {
                Component component =
                    manager.createComponent().setImplementation(new ResourceRegister(entry, defaultPages))
                        .add(manager.createServiceDependency().setService(HttpService.class).setRequired(true))
                        .add(manager.createServiceDependency().setService(LogService.class).setRequired(false));

                if ((contextId != null) && !"".equals(contextId)) {
                    component.add(manager.createServiceDependency()
                        .setService(HttpContext.class, "(" + CONTEXTID + "=" + contextId + ")").setRequired(true));
                }

                m_components.putIfAbsent(bundle, manager);

                manager.add(component);
            }
        }
    }

    /**
     * Removes a given bundle as resource provider to the HTTP context.
     * 
     * @param bundle
     *        the bundle to remove as resource provider, cannot be
     *        <code>null</code>.
     */
    public void removeResourceBundle(Bundle bundle) {
        DependencyManager dm = m_components.remove(bundle);
        if (dm != null) {
            dm.clear();
        }
    }

    @Override
    @SuppressWarnings("rawtypes")
    public void updated(Dictionary properties) throws ConfigurationException {
        List<DependencyManager> dms = new ArrayList<DependencyManager>(m_components.values());
        for (DependencyManager dm : dms) {
            List comps = dm.getComponents();
            for (Object comp : comps) {
                Object service = ((Component) comp).getService();
                if (service instanceof ManagedService) {
                    ((ManagedService) service).updated(properties);
                }
            }
        }
    }

    void warn(String message) {
        if (m_log != null) {
            m_log.log(LogService.LOG_WARNING, message);
        }
    }
}
