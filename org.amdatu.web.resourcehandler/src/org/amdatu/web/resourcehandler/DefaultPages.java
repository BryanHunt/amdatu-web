/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Provides a container for the default pages.
 */
public class DefaultPages {
    private static final String SLASH = "/";

    private final SortedMap<String, List<String>> m_defaultPages;
    private final List<String> m_globalDefault;

    /**
     * Creates a new {@link DefaultPages} instance.
     */
    public DefaultPages() {
        m_defaultPages = new TreeMap<String, List<String>>(new LengthComparator());
        m_globalDefault = new ArrayList<String>();
    }

    /**
     * Adds a default page for the given path.
     * 
     * @param path the path to add the default page for, cannot be <code>null</code>;
     * @param page the default page to add, cannot be <code>null</code> or empty.
     * @throws IllegalArgumentException in case one of the given arguments was <code>null</code>.
     */
    public void addDefault(String path, String page) {
        if (path == null) {
            throw new IllegalArgumentException("Path cannot be null or empty!");
        }
        if (page == null || "".equals(page.trim())) {
            throw new IllegalArgumentException("Page cannot be null or empty!");
        }

        path = appendSlash(path);
        List<String> pages = m_defaultPages.get(path);
        if (pages == null) {
            pages = new ArrayList<String>();
            m_defaultPages.put(path, pages);
        }
        addToCollection(pages, page);
    }

    /**
     * Add a given page to the list of "global" defaults.
     * 
     * @param page the page to add as global default, cannot be <code>null</code>.
     * @throws IllegalArgumentException in case the given page was <code>null</code> or empty.
     */
    public void addGlobalDefault(String page) {
        addToCollection(m_globalDefault, page);
    }

    /**
     * Returns a collection of default pages for the given path.
     * 
     * @param path the path to get the default pages for, cannot be <code>null</code>.
     * @return the collection with default pages for the given path, never <code>null</code>, but can be empty.
     * @throws IllegalArgumentException in case the given path was <code>null</code>.
     */
    public List<String> getDefaultPagesFor(String path) {
        if (path == null) {
            throw new IllegalArgumentException("Path cannot be null!");
        }
        String pathSlash = appendSlash(path);

        List<String> pages = null;
        for (String key : m_defaultPages.keySet()) {
            if (key.equals(path) || pathSlash.equals(key)) {
                pages = new ArrayList<String>();
                for (String defaultPage : m_defaultPages.get(key)) {
                    addPermutations(pages, pathSlash, defaultPage);
                }
                break;
            }
        }
        if (pages == null) {
            pages = new ArrayList<String>();
            for (String globalDefault : m_globalDefault) {
                addPermutations(pages, pathSlash, globalDefault);
            }
        }

        return pages;
    }

    /**
     * Adds several permutations of the path(-parts) and given default page.
     */
    private void addPermutations(List<String> pages, String path, String defaultPage) {
        if (SLASH.equals(path)) {
            pages.add(path.concat(defaultPage));
        }
        else {
            String[] parts = path.split(SLASH);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < parts.length; i++) {
                sb.append(parts[i]).append(SLASH);
                String page = sb.toString().concat(defaultPage);
                pages.add(0, page);
            }
        }
    }

    /**
     * Adds a given page to a given list if it does not already exist in the given list.
     * 
     * @param pages the pages to add the given page to;
     * @param page the page to add, cannot be <code>null</code> or empty.
     */
    private void addToCollection(List<String> pages, String page) {
        page = (page == null) ? "" : page.trim();
        if ("".equals(page)) {
            throw new IllegalArgumentException("Page cannot be null or empty!");
        }

        if (!pages.contains(page)) {
            pages.add(page);
        }
    }

    /**
     * Appends a trailing slash to the given value, if it does not already contain a trailing slash.
     */
    private String appendSlash(String value) {
        if (!value.endsWith(SLASH)) {
            return value.concat(SLASH);
        }
        return value;
    }

    /**
     * Sorts strings on descending order.
     */
    static final class LengthComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            return o1.length() - o2.length();
        }
    }
}
