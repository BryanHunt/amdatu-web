/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler;

import static org.amdatu.web.resourcehandler.Constants.CACHE_TIMEOUT_DISABLED;
import static org.amdatu.web.resourcehandler.Constants.HTTP_CACHE_CONTROL;
import static org.amdatu.web.resourcehandler.Constants.HTTP_IF_MODIFIED_SINCE;
import static org.amdatu.web.resourcehandler.Constants.HTTP_LAST_MODIFIED;
import static org.amdatu.web.resourcehandler.Constants.ONE_WEEK_IN_SECONDS;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.framework.Bundle;

/**
 * Handles resources for the QTI-player.
 */
public class ResourceServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final int BUFFER_SIZE = 4096;
    private static final String SLASH = "/";

    private final Bundle m_bundle;
    private final DefaultPages m_defaultPages;
    private final ResourceEntry m_resourceEntry;
    private final AtomicLong m_cacheTimeoutRef;

    /**
     * Creates a new {@link ResourceServlet} instance.
     * 
     * @param bundle the bundle for which to register the resource servlet, cannot be <code>null</code>;
     * @param resourceEntry the resource information, cannot be <code>null</code>;
     * @param defaultPages the default pages to use, cannot be <code>null</code>.
     */
    public ResourceServlet(Bundle bundle, ResourceEntry resourceEntry, DefaultPages defaultPages) {
        if (bundle == null) {
            throw new IllegalArgumentException("Bundle cannot be null!");
        }
        if (resourceEntry == null) {
            throw new IllegalArgumentException("ResourceEntry cannot be null!");
        }
        if (defaultPages == null) {
            throw new IllegalArgumentException("DefaultPages cannot be null!");
        }

        m_bundle = bundle;
        m_resourceEntry = resourceEntry;
        m_defaultPages = defaultPages;

        m_cacheTimeoutRef = new AtomicLong(ONE_WEEK_IN_SECONDS);
    }

    /**
     * Sets the cache timeout for this servlet.
     * 
     * @param cacheTimeout the timeout, in seconds, for resources served by this servlet.
     *        Negative values will disable adding cache headers.
     */
    public void setCacheTimeout(long cacheTimeout) {
        long oldValue;
        do {
            oldValue = m_cacheTimeoutRef.get();
        } while (!m_cacheTimeoutRef.compareAndSet(oldValue, cacheTimeout));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        URL url = null;

        String pathInfo = request.getPathInfo();
        String[] resourceNames = getResourceNamesFor(pathInfo);
        boolean showDefaultPage = false;

        for (int j = 0; (url == null) && j < resourceNames.length; j++) {
            String resource = resourceNames[j];

            // Try to determine whether we're given a resource with an actual file, or that
            // it is pointing to an (internal) directory. In the latter case, use the default
            // pages to search instead...
            if (m_bundle.findEntries(resource, "*", false /* recurse */) == null) {
                // Not a directory, may be a real file?
                url = m_bundle.getResource(resource);
            } else {
                // Given resource was a directory, stop looking for the actual resource
                // and check whether we can display a default page instead...
                showDefaultPage = true;
                break;
            }
        }

        if ((url == null) && showDefaultPage) {
            List<String> defaultPages = m_defaultPages.getDefaultPagesFor(pathInfo);

            for (int i = 0, size = defaultPages.size(); (url == null) && (i < size); i++) {
                String[] defaultPageNames = getResourceNamesFor(defaultPages.get(i));
                if (defaultPageNames != null) {
                    url = m_bundle.getResource(defaultPageNames[0]);
                }
            }
        }

        if (url != null) {
            streamResource(request, response, url);
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    private void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                // Ignore...
            }
        }
    }

    /**
     * Very crude way of determining what kind of content we've got, based on file
     * extensions.
     * 
     * @param resourceName the name of the resource to determine the content type for,
     *        cannot be <code>null</code>.
     * @return the content type for the resource, never <code>null</code>.
     */
    private String determineContentType(String resourceName) {
        String mimetype = getServletContext().getMimeType(resourceName);
        if (mimetype == null) {
            mimetype = "application/octet-stream";

            if (resourceName.endsWith(".json")) {
                mimetype = "application/json";
            }
        }

        return mimetype;
    }

    /**
     * Tries to determine the last modified date of the given URL-connection.
     * 
     * @param conn the URL-connection to get the last modified date from, cannot be
     *        <code>null</code>.
     * @return the last modified date, as epoch time.
     */
    private long getLastModified(URLConnection conn) {
        long lastModified = conn.getLastModified();

        if (lastModified == 0) {
            // Unknown by the connection, let's try to ask it directly from the
            // File-object (all resources are present as physical file)...
            String filepath = conn.getURL().getPath();
            if (filepath != null) {
                File f = new File(filepath);
                if (f.exists()) {
                    lastModified = f.lastModified();
                }
            }
        }

        return lastModified;
    }

    /**
     * Returns the various "fully qualified" resource names for a given resource name.
     * 
     * @param name the name of the resource to get the various permutations for,
     *        cannot be <code>null</code>.
     * @return an array with "fully qualified" permutations of the given resource name,
     *         or <code>null</code> if no permutations could be made.
     */
    private String[] getResourceNamesFor(String name) {
        List<String> result = new ArrayList<String>();

        for (String path : m_resourceEntry.getPaths()) {
            StringBuilder sb = new StringBuilder();
            sb.append(path);

            if (sb.indexOf(SLASH) != sb.length() && !name.startsWith(SLASH)) {
                sb.append(SLASH);
            }
            sb.append(name);

            result.add(sb.toString());
        }

        return result.size() == 0 ? null : result.toArray(new String[result.size()]);
    }

    /**
     * Determines whether the resource is modified since a given timestamp.
     * 
     * @param lastModifiedDate the date on which the resource was modified last (epoch
     *        time in milliseconds);
     * @param modifiedSinceDate the date to check against (epoch time in milliseconds).
     * @return <code>true</code> if the modified more recently than the given timestamp,
     *         <code>false</code> otherwise.
     */
    private boolean isModifiedSince(long lastModifiedDate, long modifiedSinceDate) {
        // Use a granularity of seconds for this check...
        modifiedSinceDate /= 1000;
        lastModifiedDate /= 1000;

        return (lastModifiedDate == 0) || (modifiedSinceDate <= 0) || (lastModifiedDate > modifiedSinceDate);
    }

    /**
     * Streams a generic resource to the requesting client.
     * 
     * @param request the request of the client, cannot be <code>null</code>;
     * @param response the response to the client, cannot be <code>null</code>;
     * @param url the URL of the resource to return, cannot be <code>null</code>.
     * @throws IOException in case of I/O problems.
     */
    private void streamResource(HttpServletRequest request, HttpServletResponse response, URL url) throws IOException {
        URLConnection conn = url.openConnection();

        long lastModifiedDate = getLastModified(conn);
        long cacheTimeout = m_cacheTimeoutRef.get();
        long ifModifiedSinceDate = request.getDateHeader(HTTP_IF_MODIFIED_SINCE);

        response.setDateHeader(HTTP_LAST_MODIFIED, lastModifiedDate);

        if (cacheTimeout != CACHE_TIMEOUT_DISABLED) {
            // Allow resources to be cached for a while before they must be revalidated...
            response.addHeader(HTTP_CACHE_CONTROL, String.format("max-age=%d, must-revalidate", cacheTimeout));
        }

        if (isModifiedSince(lastModifiedDate, ifModifiedSinceDate)) {
            response.setContentType(determineContentType(url.getFile()));
            response.setContentLength(conn.getContentLength());
            // we're streaming a file, so we should use the OS's character encoding...
            response.setCharacterEncoding(Charset.defaultCharset().name());

            streamResource(conn, response.getOutputStream());
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
        }
    }

    /**
     * Streams the contents of the given URL connection verbatimly to the given output
     * stream.
     * 
     * @param conn the URL connection to stream from, cannot be <code>null</code>;
     * @param os the output stream to stream to, cannot be <code>null</code>.
     * @throws IOException in case of I/O problems.
     */
    private void streamResource(URLConnection conn, OutputStream os) throws IOException {
        byte[] buf = new byte[BUFFER_SIZE];
        InputStream is = null;

        try {
            is = conn.getInputStream();

            int n;
            while ((n = is.read(buf, 0, buf.length)) >= 0) {
                os.write(buf, 0, n);
            }
        } finally {
            closeSilently(os);
            closeSilently(is);
        }
    }
}
