/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler;

import java.util.Collection;

import junit.framework.TestCase;

/**
 * Test cases for {@link DefaultPages}.
 */
public class DefaultPagesTest extends TestCase {

    public void testAddGlobalEmptyPageFail() throws Exception {
        try {
            new DefaultPages().addGlobalDefault(" ");
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException e) {
            // Ok; expected...
        }
    }

    public void testAddGlobalNullPageFail() throws Exception {
        try {
            new DefaultPages().addGlobalDefault(null);
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException e) {
            // Ok; expected...
        }
    }

    public void testAddGlobalPageIgnoresDuplicateOk() throws Exception {
        DefaultPages defaultPages = new DefaultPages();
        defaultPages.addGlobalDefault("a");
        defaultPages.addGlobalDefault("a");

        Collection<String> pages = defaultPages.getDefaultPagesFor("");
        assertEquals(1, pages.size());
        assertTrue(pages.contains("/a"));
    }

    public void testAddGlobalPageOk() throws Exception {
        DefaultPages defaultPages = new DefaultPages();
        defaultPages.addGlobalDefault("a");

        Collection<String> pages = defaultPages.getDefaultPagesFor("");
        assertEquals(1, pages.size());
        assertTrue(pages.contains("/a"));
    }

    public void testAddLocalEmptyPageFail() throws Exception {
        try {
            new DefaultPages().addDefault("", " ");
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException e) {
            // Ok; expected...
        }
    }

    public void testAddLocalMultiplePagesOk() throws Exception {
        DefaultPages defaultPages = new DefaultPages();
        defaultPages.addDefault("", "a");
        defaultPages.addDefault("", "b");

        Collection<String> pages = defaultPages.getDefaultPagesFor("");
        assertEquals(2, pages.size());
        assertTrue(pages.contains("/a"));
        assertTrue(pages.contains("/b"));
    }

    public void testAddLocalNullPageFail() throws Exception {
        try {
            new DefaultPages().addDefault("", null);
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException e) {
            // Ok; expected...
        }
    }

    public void testAddLocalNullPathFail() throws Exception {
        try {
            new DefaultPages().addDefault(null, "foo");
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException e) {
            // Ok; expected...
        }
    }

    public void testAddLocalPagesIgnoresDuplicateOk() throws Exception {
        DefaultPages defaultPages = new DefaultPages();
        defaultPages.addDefault("/a", "b");
        defaultPages.addDefault("/a/", "b");

        Collection<String> pages;

        pages = defaultPages.getDefaultPagesFor("/a");
        assertTrue(pages.contains("/a/b"));

        pages = defaultPages.getDefaultPagesFor("/a/");
        assertTrue(pages.contains("/a/b"));

        pages = defaultPages.getDefaultPagesFor("/b");
        assertEquals(0, pages.size());
    }

    public void testGetDefaultPageForNullPathFail() throws Exception {
        try {
            new DefaultPages().getDefaultPagesFor(null);
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException e) {
            // Ok; expected...
        }
    }
}
