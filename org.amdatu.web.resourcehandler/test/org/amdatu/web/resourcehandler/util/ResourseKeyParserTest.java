/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler.util;

import static org.amdatu.web.resourcehandler.util.ResourceKeyParser.getEntries;

import java.util.Collection;
import java.util.List;

import junit.framework.TestCase;

import org.amdatu.web.resourcehandler.ResourceEntry;
import org.amdatu.web.resourcehandler.ResourceEntryMap;

/**
 * Test cases for {@link ResourceKeyParser}.
 */
public class ResourseKeyParserTest extends TestCase {
    public void testAliasOnlyOk() throws Exception {
        ResourceEntryMap entries = getEntries("path");

        List<ResourceEntry> list = entries.getEntry("");
        ResourceEntry item = list.get(0);

        assertEquals("/path", item.getAlias());
        assertPaths(item, "/path");
    }

    public void testAliasPath() throws Exception {
        String entryText = "alias ; path";
        ResourceEntryMap entries = getEntries(entryText);

        Collection<String> contextIDs = entries.getContextIDs();
        assertEquals(1, contextIDs.size());
        assertTrue(contextIDs.contains(""));
    }

    public void testAliasPathContext() throws Exception {
        String entryText = "alias; path; context";
        ResourceEntryMap entries = getEntries(entryText);

        Collection<String> contextIDs = entries.getContextIDs();
        assertEquals(1, contextIDs.size());
        assertTrue(contextIDs.contains("context"));
    }

    public void testCoercePathsForSameAliasOk() throws Exception {
        String entryText = "alias1;/path1, alias1;/path2, alias2;/path3, alias1;/path4";
        ResourceEntryMap entries = getEntries(entryText);

        List<ResourceEntry> list = entries.getEntry("");
        assertEquals(2, list.size());

        ResourceEntry entry;

        entry = list.get(0);
        assertEquals("/alias1", entry.getAlias());
        assertPaths(entry, "/path1", "/path2", "/path4");

        entry = list.get(1);
        assertEquals("/alias2", entry.getAlias());
        assertPaths(entry, "/path3");
    }

    public void testEmptyEntryOk() throws InvalidEntryException {
        ResourceEntryMap entries = getEntries(" ");
        assertTrue(entries.getContextIDs().isEmpty());
    }

    public void testEntryWithoutAliasAndPathFail() throws InvalidEntryException {
        String entry = " ; ; ";

        try {
            getEntries(entry);
            fail("InvalidEntryException expected!");
        }
        catch (InvalidEntryException exception) {
            // Ok; expected...
        }
    }

    public void testEntryWithoutAliasFail() throws InvalidEntryException {
        String entry = " ;path;context";

        try {
            getEntries(entry);
            fail("InvalidEntryException expected!");
        }
        catch (InvalidEntryException exception) {
            // Ok; expected...
        }
    }

    public void testEntryWithoutPathFail() throws InvalidEntryException {
        String entry = "alias;;context";

        try {
            getEntries(entry);
            fail("InvalidEntryException expected!");
        }
        catch (InvalidEntryException exception) {
            // Ok; expected...
        }
    }

    public void testFailEntry() throws InvalidEntryException {
        String entry = "path;path;path;path";

        try {
            getEntries(entry);
            fail("InvalidEntryException expected!");
        }
        catch (InvalidEntryException exception) {
            // Ok; expected...
        }
    }

    public void testMulipleContextsAndAliassesAndPaths() throws Exception {
        String entryText = "alias1;path1;context1, alias1;/path2;context2, alias2;/path3, alias3;path2;context1, ";
        ResourceEntryMap entries = getEntries(entryText);

        List<ResourceEntry> list;
        ResourceEntry item;

        list = entries.getEntry("context1");
        assertEquals(2, list.size());

        item = list.get(0);
        assertEquals("/alias1", item.getAlias());
        assertPaths(item, "/path1");

        item = list.get(1);
        assertEquals("/alias3", item.getAlias());
        assertPaths(item, "/path2");

        list = entries.getEntry("");
        assertEquals(1, list.size());

        item = list.get(0);
        assertEquals("/alias2", item.getAlias());
        assertPaths(item, "/path3");

        list = entries.getEntry("context2");
        assertEquals(1, list.size());

        item = list.get(0);
        assertEquals("/alias1", item.getAlias());
        assertPaths(item, "/path2");
    }

    public void testNullEntryOk() throws InvalidEntryException {
        ResourceEntryMap entries = getEntries(null);
        assertTrue(entries.getContextIDs().isEmpty());
    }

    private void assertPaths(ResourceEntry entry, String... expectedPaths) {
        List<String> paths = entry.getPaths();
        assertEquals("Not all paths found,", expectedPaths.length, paths.size());
        for (String expectedPath : expectedPaths) {
            assertTrue("Path not found: " + expectedPath, paths.contains(expectedPath));
        }
    }
}
