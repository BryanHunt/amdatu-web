/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler.util;

import org.amdatu.web.resourcehandler.util.ResourceKeyParser.Entry;

import junit.framework.TestCase;

/**
 * Test cases for RegisterItem.
 */
public class EntryTest extends TestCase {

    public void testCreateRegisterItemWithAliasAndPathOnlyOk() throws Exception {
        Entry item = new Entry("alias  ;path  ");
        assertNotNull(item);

        assertEquals("/alias", item.m_alias);
        assertEquals("/path", item.m_path);
        assertEquals("", item.m_contextId);
    }

    public void testCreateRegisterItemWithAliasOnlyOk() throws Exception {
        Entry item = new Entry("alias  ");
        assertNotNull(item);

        assertEquals("/alias", item.m_alias);
        assertEquals("/alias", item.m_path);
        assertEquals("", item.m_contextId);
    }

    public void testCreateRegisterItemWithAliasPathAndContextIdOk() throws Exception {
        Entry item = new Entry("alias;/path;contextId");
        assertNotNull(item);

        assertEquals("/alias", item.m_alias);
        assertEquals("/path", item.m_path);
        assertEquals("contextId", item.m_contextId);
    }

    public void testCreateRegisterItemWithEmptyAliasFail() throws Exception {
        try {
            new Entry(";b;c");
            fail("InvalidEntryException expected!");
        }
        catch (InvalidEntryException e) {
            // Ok; expected...
        }
    }

    public void testCreateRegisterItemWithEmptyContextIdOk() throws Exception {
        Entry item = new Entry("alias;path;");
        assertNotNull(item);

        assertEquals("/alias", item.m_alias);
        assertEquals("/path", item.m_path);
        assertEquals("", item.m_contextId);
    }

    public void testCreateRegisterItemWithEmptyPathFail() throws Exception {
        try {
            new Entry("a; ;c");
            fail("InvalidEntryException expected!");
        }
        catch (InvalidEntryException e) {
            // Ok; expected...
        }
    }

    public void testCreateRegisterItemWithFourArgsFail() throws Exception {
        try {
            new Entry("a;b;c;d");
            fail("InvalidEntryException expected!");
        }
        catch (InvalidEntryException e) {
            // Ok; expected...
        }
    }

    public void testCreateRegisterItemWithNullAliasFail() throws Exception {
        try {
            new Entry(";b;c");
            fail("InvalidEntryException expected!");
        }
        catch (InvalidEntryException e) {
            // Ok; expected...
        }
    }

    public void testCreateRegisterItemWithNullArgsFail() throws Exception {
        try {
            new Entry(null);
            fail("InvalidEntryException expected!");
        }
        catch (InvalidEntryException e) {
            // Ok; expected...
        }
    }

    public void testCreateRegisterItemWithNullPathFail() throws Exception {
        try {
            new Entry("a;;c");
            fail("InvalidEntryException expected!");
        }
        catch (InvalidEntryException e) {
            // Ok; expected...
        }
    }

    public void testCreateRegisterItemWithZeroArgsFail() throws Exception {
        try {
            new Entry(";");
            fail("InvalidEntryException expected!");
        }
        catch (InvalidEntryException e) {
            // Ok; expected...
        }
    }
}
