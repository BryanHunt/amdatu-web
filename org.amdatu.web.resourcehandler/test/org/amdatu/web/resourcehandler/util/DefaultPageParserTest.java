/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler.util;

import static org.amdatu.web.resourcehandler.util.DefaultPageParser.parseDefaultPages;

import java.util.Collection;

import junit.framework.TestCase;

import org.amdatu.web.resourcehandler.DefaultPages;

/**
 * Test cases for {@link DefaultPageParser}.
 */
public class DefaultPageParserTest extends TestCase {

    public void testAddGlobalAndLocalDefaultOk() throws Exception {
        DefaultPages defaultPages = parseDefaultPages("default.html, /a=index.html");
        Collection<String> pages;

        pages = defaultPages.getDefaultPagesFor("/");
        assertTrue(pages.contains("/default.html"));

        pages = defaultPages.getDefaultPagesFor("/a");
        assertTrue(pages.contains("/a/index.html"));

        pages = defaultPages.getDefaultPagesFor("/a/b");
        assertTrue(pages.contains("/a/default.html"));
        assertTrue(pages.contains("/a/b/default.html"));

        pages = defaultPages.getDefaultPagesFor("/c");
        assertTrue(pages.contains("/c/default.html"));
    }

    public void testAddLocalDefaultOk() throws Exception {
        DefaultPages defaultPages = parseDefaultPages("/a=index.html");
        Collection<String> pages;

        pages = defaultPages.getDefaultPagesFor("/");
        assertTrue(pages.isEmpty());

        pages = defaultPages.getDefaultPagesFor("/a");
        assertTrue(pages.contains("/a/index.html"));

        pages = defaultPages.getDefaultPagesFor("/aaa");
        assertTrue(pages.isEmpty());

        pages = defaultPages.getDefaultPagesFor("/a/b");
        assertTrue(pages.isEmpty());
    }

    public void testAddPartlyOverlappingLocalDefaultsOk() throws Exception {
        DefaultPages defaultPages = parseDefaultPages("/a=b.html, /a/b=c.html, /a/b/c=d.html, default.html");
        Collection<String> pages;

        pages = defaultPages.getDefaultPagesFor("/");
        assertTrue(pages.contains("/default.html"));

        pages = defaultPages.getDefaultPagesFor("/a");
        assertTrue(pages.contains("/a/b.html"));

        pages = defaultPages.getDefaultPagesFor("/a/b");
        assertTrue(pages.contains("/a/b/c.html"));

        pages = defaultPages.getDefaultPagesFor("/a/b/c");
        assertTrue(pages.contains("/a/b/c/d.html"));

        pages = defaultPages.getDefaultPagesFor("/a/b/c/d");
        assertTrue(pages.contains("/a/b/c/d/default.html"));

        pages = defaultPages.getDefaultPagesFor("/c");
        assertTrue(pages.contains("/c/default.html"));
    }

    public void testParseEmptyEntryFail() throws Exception {
        try {
            parseDefaultPages("default.txt, ,");
            fail("InvalidEntryException expected!");
        }
        catch (InvalidEntryException e) {
            // Ok; expected...
        }
    }

    public void testParseEmptyKeyOk() throws Exception {
        DefaultPages defaultPages = parseDefaultPages(" ");
        Collection<String> pages;

        pages = defaultPages.getDefaultPagesFor("/");
        assertTrue(pages.isEmpty());

        pages = defaultPages.getDefaultPagesFor("/a");
        assertTrue(pages.isEmpty());
    }

    public void testParseEntryWithoutPageFail() throws Exception {
        try {
            parseDefaultPages("/=");
            fail("InvalidEntryException expected!");
        } catch (InvalidEntryException exception) {
            // Ok; expected...
        }
    }

    public void testParseEntryWithoutPathOk() throws Exception {
        DefaultPages defaultPages = parseDefaultPages("=index.html");

        Collection<String> pages = defaultPages.getDefaultPagesFor("");
        assertTrue(pages.contains("/index.html"));
    }

    public void testParseGlobalEntryOk() throws Exception {
        DefaultPages defaultPages = parseDefaultPages("index.html");

        Collection<String> pages = defaultPages.getDefaultPagesFor("/");
        assertTrue(pages.contains("/index.html"));

        pages = defaultPages.getDefaultPagesFor("/a");
        assertTrue(pages.contains("/a/index.html"));
    }

    public void testParseNullKeyOk() throws Exception {
        DefaultPages defaultPages = parseDefaultPages(null);
        Collection<String> pages;

        pages = defaultPages.getDefaultPagesFor("/");
        assertTrue(pages.isEmpty());

        pages = defaultPages.getDefaultPagesFor("/a");
        assertTrue(pages.isEmpty());
    }
}
