/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.amdatu.web.rest.doc.Description;

@Path("hello")
@Description("Demonstrates how to create REST endpoints.")
public class HelloWorldResource {
    private volatile User m_lastUser = new User("John", "Doe");
    @Context
    private HttpServletRequest request;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Description("Will return a friendly message from your server.")
    public String helloPlain() {
        return "hello world";
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Description("Will return a friendly message from your server.")
    public String helloJSON() {
        return "\"hello world\"";
    }

    @GET
    @Path("context")
    @Produces("text/plain")
    @Description("Returns a message with some context information")
    public String hello2(@Context UriInfo uriInfo, @Context HttpServletRequest request) {
        return "Hello world from " + uriInfo.getAbsolutePath();
    }

    @GET
    @Path("user")
    @Produces(MediaType.APPLICATION_JSON)
    @Description("Returns the first and last name of the user.")
    public User user() {
        return m_lastUser;
    }

    @POST
    @Path("user")
    @Consumes(MediaType.APPLICATION_JSON)
    @Description("Sets the first and last name for the user, needs a JSON object with the fields 'first' and 'last'.")
    public void user(User user) {
        m_lastUser = user;
    }
}

class User {
    public User() {
    }

    public User(String first, String last) {
        this.first = first;
        this.last = last;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    private String first;
    private String last;

    @Override
    public String toString() {
        return "User[" + first + " " + last + "]";
    }
}
